package com.example.appapirecipes;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONStringer;

public class MainActivity extends AppCompatActivity {
    private TextView tvBusqueda;
    private TextView tvIngredientes;
    private Button btBuscar;
    private TextView tvlabel;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.tvBusqueda = (TextView) findViewById(R.id.EtBuscar);
        this.btBuscar = (Button) findViewById(R.id.BtBuscar);
        this.tvIngredientes = (TextView) findViewById(R.id.tvIngredientes);
        this.tvlabel = (TextView) findViewById(R.id.tvLabel);

        this.btBuscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String b = tvBusqueda.getText().toString().trim();
                String url = "https://api.edamam.com/search?q="+ b +"&app_id=9b1b7a71&app_key=d424a6869b60ee48426306eb74b72b3d";
                StringRequest solicitud = new StringRequest(
                        Request.Method.GET,
                        url,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                // Tenemos respuesta desde el servidor
                                try {
                                    JSONObject respuestaJSON = new JSONObject(response);


                                    JSONArray hitsJSON = respuestaJSON.getJSONArray("hits");
                                    JSONObject primer = hitsJSON.getJSONObject(0);
                                    JSONObject recipesJSON  = primer.getJSONObject("recipe");
                                    JSONArray in = recipesJSON.getJSONArray("ingredientLines");
                                    String label = recipesJSON.getString("label");


                                    String ingre = "" ;

                                    for(int i = 0; i<in.length();i++)
                                    {


                                        ingre = ingre + in.getString(i)+ "\n" ;


                                    }
                                    tvlabel.setText(label);
                                    tvIngredientes.setText(ingre);


                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                // Error
                            }
                        }
                );

                RequestQueue listaEspera = Volley.newRequestQueue(getApplicationContext());
                listaEspera.add(solicitud);
            }
        });
    }
}
